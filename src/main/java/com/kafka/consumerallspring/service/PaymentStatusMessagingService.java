package com.kafka.consumerallspring.service;

import com.kafka.consumerallspring.dto.response.PaymentStatusMessageResponseDTO;

import java.util.List;

public interface PaymentStatusMessagingService {

    List<PaymentStatusMessageResponseDTO> getMessages();

    void clearCache();
}
