package com.kafka.consumerallspring.messaging;

import com.kafka.consumerallspring.dto.response.PaymentStatusMessageResponseDTO;
import com.kafka.consumerallspring.messaging.schema.PaymentStatusMessage;
import com.kafka.consumerallspring.messaging.storage.MessageStorage;
import com.kafka.consumerallspring.service.PaymentStatusMessagingService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class PaymentStatusMessagingServiceImpl implements PaymentStatusMessagingService {

    @Autowired
    MessageStorage messageStorage;

    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public List<PaymentStatusMessageResponseDTO> getMessages() {
        return messageStorage.getStorage().stream()
                .map(PaymentStatusMessageResponseDTO::build)
                .collect(toList());
    }

    @Override
    public void clearCache() {
        this.messageStorage.clear();
    }


    @KafkaListener(
            topicPartitions = {
                    @TopicPartition(topic = "${kafka.topic}", partitionOffsets = @PartitionOffset(partition = "0", initialOffset = "0")),
                    @TopicPartition(topic = "${kafka.topic}", partitionOffsets = @PartitionOffset(partition = "1", initialOffset = "0"))
            }
    )
    public void listen(ConsumerRecord<String, PaymentStatusMessage> record) {
        log.debug("Received message Offset = {}, Key = {}, Value = {} from partition {}",
                record.offset(), record.key(), record.value(), record.partition());
        messageStorage.add(record);
    }
}
