package com.kafka.consumerallspring.messaging.storage;

import com.kafka.consumerallspring.messaging.schema.PaymentStatusMessage;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageStorage {

    List<ConsumerRecord<String, PaymentStatusMessage>> storage;

    public MessageStorage() {
        storage = new ArrayList<>();
    }

    public void add(ConsumerRecord<String, PaymentStatusMessage> message) {
        storage.add(message);
    }

    public List<ConsumerRecord<String, PaymentStatusMessage>> getStorage() {
        return storage;
    }

    public void clear() {
        storage.clear();
    }
}
