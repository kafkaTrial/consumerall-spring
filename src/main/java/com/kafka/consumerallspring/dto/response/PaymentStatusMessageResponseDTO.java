package com.kafka.consumerallspring.dto.response;

import com.kafka.consumerallspring.messaging.schema.PaymentStatusMessage;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.Objects;
import java.util.StringJoiner;

public class PaymentStatusMessageResponseDTO {
    private Long partition;
    private Long offset;
    private String key;

    public Long getPartition() {
        return partition;
    }

    public void setPartition(Long partition) {
        this.partition = partition;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private String superPnr;
    private String paymentId;
    private String status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentStatusMessageResponseDTO that = (PaymentStatusMessageResponseDTO) o;
        return Objects.equals(superPnr, that.superPnr) &&
                Objects.equals(paymentId, that.paymentId) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {

        return Objects.hash(superPnr, paymentId, status);
    }

    public String getSuperPnr() {

        return superPnr;
    }

    public void setSuperPnr(String superPnr) {
        this.superPnr = superPnr;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public static PaymentStatusMessageResponseDTO build(ConsumerRecord<String, PaymentStatusMessage> paymentStatusMessage) {
        PaymentStatusMessageResponseDTO paymentStatusMessageResponseDTO = new PaymentStatusMessageResponseDTO();
        paymentStatusMessageResponseDTO.setPaymentId(paymentStatusMessage.value().getPaymentId().toString());
        paymentStatusMessageResponseDTO.setSuperPnr(paymentStatusMessage.value().getSuperPnr().toString());
        paymentStatusMessageResponseDTO.setStatus(paymentStatusMessage.value().getStatus().toString());
        paymentStatusMessageResponseDTO.setKey(paymentStatusMessage.key());
        paymentStatusMessageResponseDTO.setPartition((long) paymentStatusMessage.partition());
        paymentStatusMessageResponseDTO.setOffset(paymentStatusMessage.offset());
        return paymentStatusMessageResponseDTO;
    }



    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("key = " + key)
                .add("offset = " + offset)
                .add("partition = " + partition)
                .add("paymentId = " + paymentId)
                .add("status = " + status)
                .add("superPnr = " + superPnr)
                .toString();
    }
}
